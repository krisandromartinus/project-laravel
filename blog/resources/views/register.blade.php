<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/register" method="post">
        @csrf
        <label>First name:</label>
        <br><br>
        <input type="text" name="firstname">
        <br>
        <br>
        <label>Last name:</label>
        <br><br>
        <input type="text" name="lastname">
        <br>
        <br>
        <label>Gender:</label>
        <br><br>
        <input type="radio" name="Gender" value="Male"> Male <br>
        <input type="radio" name="Gender" value="Female"> Female <br>
        <input type="radio" name="Gender" value="Other" > Other <br>
        <br>
        <label>Nationaly:</label>
        <br><br>
        <select name="nationaly" >
            <option value="Indonesian">Indonesian</option>
        </select>
        <br>
        <br>
        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox" name="bahasa" value="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="Bahasa English"> English <br>
        <input type="checkbox" name="bahasa" value="Other"> Other <br>
        <br>
        <br>
        <label>Bio:</label>
        <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
        <button type="submit">Sign Up</button>

    </form>

    
</body>
</html>