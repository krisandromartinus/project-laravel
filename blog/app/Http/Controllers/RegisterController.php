<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index()
    {

        return view('register');
    }

    public function postData(Request $request)
    {
        return view('success', ['nama' => $request->firstname . ' ' . $request->lastname]);
    }
}
